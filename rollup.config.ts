import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import sourceMaps from 'rollup-plugin-sourcemaps'
import typescript from 'rollup-plugin-typescript2'
import json from '@rollup/plugin-json'
import {terser} from 'rollup-plugin-terser'
// import globals from 'rollup-plugin-node-globals'
// import builtins from 'rollup-plugin-node-builtins'

const pkg = require('./package.json')

const libraryName = 'docs-dispatcher-sdk'

export default [
  // Browser build
  {
    input: `src/main.ts`,
    output: [
      { file: pkg.browser, name: 'docsDispatcher', format: 'umd', sourcemap: true },
      { file: pkg.module, format: 'es', sourcemap: true }
    ],
    // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
    external: ['axios'],
    watch: {
      include: 'src/**/**'
    },
    plugins: [
      // globals(),
      // builtins(),
      // Compile TypeScript files
      typescript({ useTsconfigDeclarationDir: true }),
      // Allow bundling cjs modules (unlike webpack, rollup doesn't understand cjs)
      commonjs(),
      // Allow node_modules resolution, so you can use 'external' to control
      // which external modules to include in the bundle
      // https://github.com/rollup/rollup-plugin-node-resolve#usage
      resolve({
        browser: true
      }),
      json(),
      // Resolve source maps to the original source
      // sourceMaps(),
      terser({
        sourcemap: true,
      })
    ]
  },

  // Config for node
  {
    input: `src/main.ts`,
    output: [
      { file: pkg.main, name: 'docsDispatcher', format: 'umd', sourcemap: true },
      { file: pkg.module, format: 'es', sourcemap: true }
    ],
    // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
    external: ['axios'],
    watch: {
      include: 'src/**/**'
    },
    plugins: [
      // globals(),
      // builtins(),
      // Compile TypeScript files
      typescript({ useTsconfigDeclarationDir: true }),
      // Allow bundling cjs modules (unlike webpack, rollup doesn't understand cjs)
      commonjs(),
      // Allow node_modules resolution, so you can use 'external' to control
      // which external modules to include in the bundle
      // https://github.com/rollup/rollup-plugin-node-resolve#usage
      resolve({
        browser: false,
        mainFields: ["module", "main"],
        preferBuiltins: true,
      }),
      json(),
      // Resolve source maps to the original source
      sourceMaps()
    ]
  }
]
