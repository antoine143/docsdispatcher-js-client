module.exports = {
  'tabWidth': 2,
  'useTabs': false,
  'arrowParens': 'avoid',
  'endOfLine': 'lf',
  'semi': false,
  'singleQuote': true,
  'printWidth': 100,
  'quoteProps': 'consistent',
  'bracketSpacing': true,
  'trailingComma': 'es5',
}
