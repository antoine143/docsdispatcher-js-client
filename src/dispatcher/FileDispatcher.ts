import { applyMixins, MixinTarget } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { TargetablePayload, TemplatePayload } from './interfaces/generics'
import TargetableMixin from './mixins/TargetableMixin'
import TemplatableMixin, { TemplatePreset } from './mixins/TemplatableMixin'

export type FileDispatcherPayload = TemplatePayload & TargetablePayload

class FileDispatcher extends BaseService<FileDispatcherPayload> {

  private basePath = '/api/file'

  public constructor(preset?: TemplatePreset) {
    super()
    if (preset) {
      this.setTemplatePreset(preset)
    }
  }

  public getAcceptedMimetypes (): string[] {
    return [
      BaseService.RETURN_FORMATS.HTML,
      BaseService.RETURN_FORMATS.TEXT,
      BaseService.RETURN_FORMATS.XLSX,
      BaseService.RETURN_FORMATS.DOCX,
      BaseService.RETURN_FORMATS.PPTX,
      BaseService.RETURN_FORMATS.SVG,
      BaseService.RETURN_FORMATS.PNG,
      BaseService.RETURN_FORMATS.JPEG,
      BaseService.RETURN_FORMATS.PDF,
      BaseService.RETURN_FORMATS.MULTIPART
    ]
  }

  public getBasePath (): string {
    return this.basePath
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.FILE
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.PDF
  }

  public asPayload (): FileDispatcherPayload {
    return {
      ...this.asTemplatePayload(),
      ...this.asTargetablePayload()
    }
  }

  public validate (_: ValidationError[]): boolean {
    throw new Error('Method not implemented')
  }

}

applyMixins(FileDispatcher, [TemplatableMixin, TargetableMixin])
interface FileDispatcher extends TemplatableMixin, TargetableMixin, MixinTarget { }

export default FileDispatcher