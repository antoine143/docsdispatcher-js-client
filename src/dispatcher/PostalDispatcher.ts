import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { AbstractData, Attachment, AttachmentCollectionPayload, TargetablePayload, TemplatePayload } from './interfaces/generics'
import TargetableMixin from './mixins/TargetableMixin'
import TemplatableMixin from './mixins/TemplatableMixin'

export interface PostalAddress {
  name: string;
  address1: string;
  address2?: string;
  address3?: string;
  address4?: string;
  city: string;
  zipCode: string;
  countryCode: string;
}

export interface PostalDispatcherPayload extends TemplatePayload, TargetablePayload {
  provider: string;
  subject: string;
  receivers: PostalAddress[];
  documents: AttachmentCollectionPayload;
  data?: AbstractData;
  // Postal options
  colorMode?: string;
}

class PostalDispatcher extends BaseService<PostalDispatcherPayload> {

  protected subject: string

  protected receivers: PostalAddress[] = []

  protected documents: Attachment[] = []

  private basePath = '/api/postal'

  public constructor(props: { subject: string }) {
    super()
    this.subject = props.subject
  }

  public getAcceptedMimetypes (): string[] {
    return [BaseService.RETURN_FORMATS.JSON]
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.POSTAL
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.JSON
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): PostalDispatcherPayload {
    return {
      ...this.asTemplatePayload(),
      ...this.asTargetablePayload(),
      provider: 'MYSENDINGBOX',
      subject: this.subject,
      receivers: this.receivers,
      documents: this.documents.map(x => x.asPayload()),
      // Postal options
      colorMode: 'BW'
    }
  }

  public addDocument (document: Attachment): this {
    this.documents.push(document)
    return this
  }

  public addReceiver (receiver: PostalAddress): this {
    this.receivers.push(receiver)
    return this
  }

  public validate (_: ValidationError[]): boolean {
    throw new Error('Method not implemented')
  }

}

applyMixins(PostalDispatcher, [TemplatableMixin, TargetableMixin])
interface PostalDispatcher extends TemplatableMixin, TargetableMixin { }

export default PostalDispatcher