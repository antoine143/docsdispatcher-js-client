import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { AbstractData, Attachment, AttachmentCollection, AttachmentCollectionPayload, TargetablePayload, TemplatePayload } from './interfaces/generics'
import HasDocumentCollections, { validateDocumentCollections } from './interfaces/HasDocumentsCollection'
import TargetableMixin from './mixins/TargetableMixin'
import TemplatableMixin from './mixins/TemplatableMixin'

export interface MailDispatcherPayload extends TemplatePayload, TargetablePayload {
  from?: string;
  to?: string[];
  cc?: string[];
  bcc?: string[];
  subject?: string;
  attachments?: AttachmentCollectionPayload;
  // String body
  body?: string;
  // Template as body
}

export interface MailDispatcherProps {
  from?: string;
  body?: string;
  subject?: string;
  templateName?: string;
  data?: AbstractData;
}
class MailDispatcher extends BaseService<MailDispatcherPayload> implements HasDocumentCollections {

  protected from?: string

  protected to: string[] = []

  protected cc: string[] = []

  protected bcc: string[] = []

  protected subject?: string

  protected body?: string

  protected attachments: Attachment[] = []

  private basePath = '/api/email'

  public constructor(props: MailDispatcherProps) {
    super()
    this.from = props.from
    this.body = props.body
    this.subject = props.subject
    this.templateName = props.templateName
    this.data = props.data
  }

  public getAttachmentCollectionsKeys (): import('./interfaces/HasDocumentsCollection').CollectionKeys {
    return ['attachments']
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.MAIL
  }

  public getAttachmentCollection (key: string): AttachmentCollection {
    switch (key) {
      case 'attachments': return this.attachments
      default: throw new Error(`Unknown attachments collection ${key}`)
    }
  }

  public getData (): import('./interfaces/generics').MaybeData {
    return this.data
  }

  public getAcceptedMimetypes (): string[] {
    return [
      BaseService.RETURN_FORMATS.PDF,
      BaseService.RETURN_FORMATS.HTML,
      BaseService.RETURN_FORMATS.TEXT,
      BaseService.RETURN_FORMATS.XLSX,
      BaseService.RETURN_FORMATS.DOCX,
      BaseService.RETURN_FORMATS.PPTX,
      BaseService.RETURN_FORMATS.SVG,
      BaseService.RETURN_FORMATS.PNG,
      BaseService.RETURN_FORMATS.JPEG,
      BaseService.RETURN_FORMATS.JSON,
    ]
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.PDF
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): MailDispatcherPayload {
    let payload = {
      ...this.asTemplatePayload(),
      ...this.asTargetablePayload(),
      from: this.from,
      to: this.to,
      cc: this.cc,
      bcc: this.bcc,
      attachments: this.attachments.length ? this.attachments.map(x => x.asPayload()) : void 0
    }
    if (!this.isTemplatable()) {
      payload = Object.assign(payload, {subject: this.subject, body: this.body})
    }
    return payload
  }

  public attach (attachment: Attachment): this {
    this.attachments.push(attachment)
    return this
  }

  public addTo (address: string[] | string): this {
    this.to = this.to.concat(address)
    return this
  }

  public addCc (address: string[] | string): this {
    this.cc = this.cc.concat(address)
    return this
  }

  public addBcc (address: string[] | string): this {
    this.bcc = this.bcc.concat(address)
    return this
  }

  public setBody (arg0: string): this {
    this.body = arg0
    return this
  }

  public setSubject (arg0: string): this {
    this.subject = arg0
    return this
  }

  public validate (errors: ValidationError[]): boolean {
    return validateDocumentCollections(this, errors)
  }
}

applyMixins(MailDispatcher, [TemplatableMixin, TargetableMixin])
interface MailDispatcher extends TemplatableMixin, TargetableMixin { }

export default MailDispatcher