import { applyMixins } from '../../util/apply-mixins'
import TemplatableMixin, { TemplatePreset } from '../mixins/TemplatableMixin'
import TargetableMixin, { TargetablePreset } from '../mixins/TargetableMixin'

export type AbstractData = Record<string, unknown>
export type MaybeData = AbstractData | undefined

export interface HasData {
  setData(data: AbstractData): unknown;
}

export type Payload = {}

export type TemplatePayload = Payload & {
  templateName?: string;
  resultFileName?: string;
  data?: AbstractData;
}

export interface Templatable extends HasData {
  setTemplateName(templateName: string): this;
  setResultFileName(resultFileName: string): this;
}

export type Target = {
  target: string;
  [key: string]: unknown;
};

export type TargetablePayload = Payload & {
  targets?: Target[];
}

export interface Targetable {
  addTarget(target: Target): this;
  addTargets(targets: Target[]): this;
}

export type AttachmentPayload = TemplatePayload & TargetablePayload

export type AttachmentCollectionPayload = AttachmentPayload[]

export class Attachment implements PayloadGenerator<AttachmentPayload> {
  public constructor(preset: TemplatePreset & TargetablePreset) {
    if (preset) {
      this.setTemplatePreset(preset)
      this.setTargetablePreset(preset)
    }
  }

  public asPayload(): AttachmentPayload {
    return { ...this.asTemplatePayload(), ...this.asTargetablePayload() }
  }
}

applyMixins(Attachment, [TemplatableMixin, TargetableMixin])
export interface Attachment extends TemplatableMixin, TargetableMixin {}

export type AttachmentCollection = Attachment[]

export interface PayloadGenerator<P> {
  asPayload(): P;
}