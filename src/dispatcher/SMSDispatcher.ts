import { applyMixins } from '../util/apply-mixins'
import { ValidationError } from '../validation/interfaces'
import BaseService, { SERVICE_TYPE } from './interfaces/BaseService'
import { AbstractData, TemplatePayload } from './interfaces/generics'
import TemplatableMixin from './mixins/TemplatableMixin'

export interface SMSDispatcherPayload extends TemplatePayload {
  from: string;
  to: string;
  provider: SMSProvider;
  smsContent?: string;
  templateName?: string;
  data?: AbstractData;
}

export interface SMSDispatcherProps {
  from: string;
  to: string;
  provider: SMSProvider;
  body?: string;
  templateName?: string;
  data?: AbstractData;
}

type SMSProvider = 'OVH' | 'MAILJET' | 'SENDINBLUE' | 'SMS_FACTOR' | 'SMS_MAGIC'

class SMSDispatcher extends BaseService<SMSDispatcherPayload>{

  private basePath = '/api/sms'

  private body?: string

  private to: string

  private from: string

  private provider: SMSProvider

  public constructor(props: SMSDispatcherProps) {
    super()
    this.body = props.body
    this.to = props.to
    this.from = props.from
    this.provider = props.provider
    this.templateName = props.templateName
    this.data = props.data
  }

  public getServiceType (): SERVICE_TYPE {
    return SERVICE_TYPE.SMS
  }

  public validate (_errors: ValidationError[]): boolean {
    return true
  }

  public getBasePath (): string {
    return this.basePath
  }

  public asPayload (): SMSDispatcherPayload {
    const { from, to, provider, body } = this
    return { from, to, provider, smsContent: body }
  }

  public getAcceptedMimetypes (): string[] {
    return [BaseService.RETURN_FORMATS.JSON]
  }

  public getDefaultFormat (): string {
    return BaseService.RETURN_FORMATS.JSON
  }

}

applyMixins(SMSDispatcher, [TemplatableMixin])
interface SMSDispatcher extends TemplatableMixin { }

export default SMSDispatcher