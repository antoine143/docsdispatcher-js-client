import ComposedService from './interfaces/ComposedService'
import { ValidationError } from '../validation/interfaces'

export default class UploadDispatcher implements ComposedService {

  private segment = '/upload'

  public getPathSegment(): string {
    return this.segment
  }

  public validate(_: ValidationError[]): boolean {
    // @todo maybe check we have at least one target. It means we should have
    // been passed the payload
    throw new Error('Method not implemented')
  }
}