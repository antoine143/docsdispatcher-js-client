import { AbstractData, MaybeData, Templatable, TemplatePayload } from '../interfaces/generics'

export type TemplatePreset = { templateName?: string; resultFileName?: string; data?: AbstractData }

export default class TemplatableMixin implements Templatable {

  protected templateName?: string

  protected resultFileName?: string

  protected data?: AbstractData

  public setResultFileName (resultFileName: string): this {
    this.resultFileName = resultFileName || 'file'
    return this
  }

  public setTemplateName (templateName: string): this {
    this.templateName = templateName
    return this
  }

  public setData (data: AbstractData): this {
    this.data = data
    return this
  }

  public getData (): MaybeData {
    return this.data
  }

  public asTemplatePayload (): TemplatePayload {
    return {
      templateName: this.templateName,
      resultFileName: this.resultFileName,
      data: this.data,
    }
  }

  public isTemplatable (): boolean{
    return Boolean(this.templateName)
  }

  protected setTemplatePreset (preset: TemplatePreset): void {
    const { templateName, resultFileName, data } = preset
    Object.assign(this, { templateName, resultFileName, data })
  }

}