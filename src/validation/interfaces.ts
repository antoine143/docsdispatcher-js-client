export type ValidationError = {
  path: Array<string | number>;
  message: string;
}