
export type ResponseType =
  | 'arraybuffer'
  | 'blob'
  | 'document'
  | 'json'
  | 'text'
  | 'stream'

export default interface RequestDescriptor {
  url: URL;
  method: 'get' | 'post' | 'put' | 'delete' | 'patch';
  headers: Record<string, string>;
  payload: Record<string, unknown>;
  responseType?: ResponseType;
}

// The preset must be the same type as RequesDescriptor with differences:
// - every field is optional
// - the url type is a string
export interface RequestDescriptorPreset {
  url?: string;
  method?: RequestDescriptor['method'];
  headers?: RequestDescriptor['headers'];
  payload?: RequestDescriptor['payload'];
  responseType?: RequestDescriptor['responseType'];
}