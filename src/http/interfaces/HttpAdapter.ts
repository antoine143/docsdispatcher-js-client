import RequestDescriptor from './RequestDescriptor'
import HttpResponse from './HttpResponse'

export default interface HttpAdapter {
  send (req: RequestDescriptor, isValidate: boolean): Promise<HttpResponse>;
}