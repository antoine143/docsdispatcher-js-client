import RequestDescriptor, { RequestDescriptorPreset, ResponseType } from './interfaces/RequestDescriptor'
import HttpRequestBuildStep from './interfaces/HttpRequestBuildStep'

export default class HttpRequestBuilder {

  protected rd: RequestDescriptor

  public constructor(preset: RequestDescriptorPreset = {}) {
    this.rd = HttpRequestBuilder.castPreset(preset)
  }

  public addAcceptedType (mimetype: string): this {
    return this.replaceHeader('accept', mimetype)
  }

  public setResponseType (rt: ResponseType): this {
    this.rd.responseType = rt
    return this
  }

  public method (method: RequestDescriptor['method']): this {
    this.rd.method = method
    return this
  }

  public setPath (path: string): this {
    this.rd.url.pathname = path
    return this
  }

  public addPathSegment (path: string): this {
    this.rd.url.pathname = this.rd.url.pathname.replace(/\/$/u, '')
      + '/'
      + path.replace(/^\//u, '')
    return this
  }

  public setPayload (payload: RequestDescriptor['payload']): this {
    this.rd.payload = payload
    return this
  }

  public putHeader (key: string, value: string): this {
    const lowKey = key.toLowerCase()

    if (typeof this.rd.headers[lowKey] !== 'undefined') {
      throw new Error(`Header '${key}' is already defined`)
    }

    return this.replaceHeader(key, value)
  }

  public replaceHeader (key: string, value: string): this {
    this.rd.headers[key.toLowerCase()] = value
    return this
  }

  public pipe (handler: HttpRequestBuildStep): this {
    return this.call(self => handler.transformRequest(self))
  }

  public call (fn: (builder: HttpRequestBuilder) => void): this {
    fn(this)
    return this
  }

  public make (): RequestDescriptor {
    return this.rd
  }

  public _rd (): RequestDescriptor {
    return this.rd
  }

  public static from (preset: RequestDescriptorPreset): HttpRequestBuilder {
    return new HttpRequestBuilder(preset)
  }

  private static castPreset (preset: RequestDescriptorPreset): RequestDescriptor {
    // Check if the url is complete
    const fakeEndpoint = 'noop://nohost'
    let url = ''
    if (preset.url) {
      if (!preset.url.match('https?://')) {
        url = fakeEndpoint + '/' + preset.url.replace(/^\//u, '')
      } else {
        url = preset.url
      }
    } else {
      url = fakeEndpoint
    }
    return {
      url: new URL(url),
      method: preset.method || 'get',
      headers: preset.headers || {},
      payload: preset.payload || {},
      responseType: preset.responseType || void 0,
    }
  }

}