import AuthAdapter from './interfaces/AuthAdapter'
import HRB from '../http/HttpRequestBuilder'

const toBase64 = typeof btoa === 'undefined'
  ? (str: string): string => Buffer.from(str).toString('base64')
  : (str: string): string => btoa(str)

// @todo AuthenticatorInterface
export default class BasicAuth implements AuthAdapter {

  private authHeader: string

  public constructor(username: string, password: string) {
    this.authHeader = `Basic ${toBase64(`${username}:${password}`)}`
  }

  public transformRequest(builder: HRB): void {
    builder.putHeader('authorization', this.authHeader)
  }

}