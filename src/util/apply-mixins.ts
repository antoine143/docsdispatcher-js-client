/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-function-return-type */

export interface MixinTarget { mixins(): void }

const mixinsSymbol: unique symbol = Symbol()
type Constructor = () => void

export function applyMixins(derivedCtor: any, baseCtors: any[]): void {
  const derivedFun = derivedCtor.prototype.constructor
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      Object.defineProperty(derivedCtor.prototype, name, Object.getOwnPropertyDescriptor(baseCtor.prototype, name) as PropertyDescriptor)
    })
  })
  derivedCtor.prototype.constructor = derivedFun
  derivedCtor.prototype[mixinsSymbol] = baseCtors as Constructor[]
}