export function asList<T>(list: T[] | undefined): T[] {
  if (list === void 0) return []
  return list
}