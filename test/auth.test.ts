import DocsDispatcher from '../src/main'
import BasicAuth from '../src/auth/BasicAuth'
import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import HttpResponse from '../src/http/interfaces/HttpResponse'
import HttpMock from './mocks/HttpMock'

describe('Authentification', () => {
  it('should pass the auth header to the request when doing an healthCheck', () => {
    const username = 'myUser'
    const password = 'myPassword'
    const expectedHeader = `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`

    const auth = new BasicAuth(username, password)

    const http = new HttpMock(function(req: RequestDescriptor) {
      expect(req.headers.authorization).toBe(expectedHeader)
      expect(req.url.toString()).toBe('https://api.docs-dispatcher.io/healthz')
      return Promise.resolve({ status: 200 } as HttpResponse)
    })

    const dd = new DocsDispatcher(auth, http)

    return dd.healthCheck()
  })
})
