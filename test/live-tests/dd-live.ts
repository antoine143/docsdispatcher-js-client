/* eslint-disable no-process-env */
import AxiosAdapter from '../../src/http/adapters/AxiosAdapter'
import axios from 'axios'
import BasicAuth from '../../src/auth/BasicAuth'
import DocsDispatcher from '../../src/main'
import {config as loadEnv} from 'dotenv'
import { Payload } from '../../src/dispatcher/interfaces/generics'

loadEnv()

describe('Check live env', () => {
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_USERNAME).toBe('string')
    expect(process.env.LIVE_USERNAME).not.toBeFalsy()
  })
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_PASSWORD).toBe('string')
    expect(process.env.LIVE_PASSWORD).not.toBeFalsy()
  })
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_ENDPOINT).toBe('string')
    expect(process.env.LIVE_ENDPOINT).not.toBeFalsy()
  })
})

export default function makeLiveDD<P extends Payload>(): DocsDispatcher<P> {

  const liveUsername = process.env.LIVE_USERNAME
  const livePassword = process.env.LIVE_PASSWORD
  const liveEndpoint = process.env.LIVE_ENDPOINT

  const auth = new BasicAuth(String(liveUsername), String(livePassword))
  const http = new AxiosAdapter(axios)

  return new DocsDispatcher<P>(auth, http, {
    endpoint: liveEndpoint
  })
}