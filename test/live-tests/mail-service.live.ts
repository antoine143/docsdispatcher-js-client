/* eslint-disable @typescript-eslint/camelcase */
import MailDispatcher from '../../src/dispatcher/MailDispatcher'
import makeLiveDD from './dd-live'
import { Attachment } from '../../src/dispatcher/interfaces/generics'

describe('Email service', () => {

  const baseData = {subject: 'Test de mail', from : 'somone@somewhere.fr', body:  'Hello from the body'}

  // it.only('should send an email without attachments', () => {
  //   return makeLiveDD()
  //     .withBaseService(new MailDispatcher(baseData))
  //     .dispatch()
  // })
  it('should send an email with attachments', async () => {

    try {
      const response = await makeLiveDD()
        .withBaseService(new MailDispatcher(baseData)
          .addTo('test@test.com')
          .attach(new Attachment({
            resultFileName: 'fichier',
            templateName: 'superadmincompany-admin-test-mail-1',
            data: {name: 'World'}
          })))
        .dispatch()
      expect(response.status).toBe(200)
      expect(response.data).toMatchObject([{status: 200, success: true}])
    } catch (e) {
      // if (e && e.response && e.response.data) console.log('e.response.data', e.response.data)
      // else console.error(e)
      expect.assertions(2)
    }
  })

  // it('should execute a payload validation', async () => {

  //   const response = await makeLiveDD()
  //     .withBaseService(new MailDispatcher(baseData)
  //       .attach(new Document({
  //         resultFileName: 'fichier',
  //         templateName: 'superadmincompany-admin-test-mail-1',
  //         data: {name: 'World'}
  //       })))
  //     .validateOnly()
  //     .dispatch()
  //   expect(response.status).toBe(200)
  //   expect(response.data).toBe('')
  // })

})
