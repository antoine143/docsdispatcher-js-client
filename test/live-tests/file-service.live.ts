import makeLiveDD from './dd-live'
import { AbstractData } from '../../src/dispatcher/interfaces/generics'
import FileDispatcher, {FileDispatcherPayload} from '../../src/dispatcher/FileDispatcher'

const FIXTURE_1 = {
  templateName: 'superadmincompany-admin-test-mail-1',
  resultFileName: 'fichier',
  data: {
    name: 'Foo',
    breadcrumb: [
      'first',
      '2nd',
      3
    ]
  }
}

describe('File service', () => {

  const fix = FIXTURE_1
  it('should create a simple file', async () => {

    let response = await makeLiveDD<FileDispatcherPayload>()
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .dispatch()
    const { data, headers } = response
    expect(data).toContain('<li>first')
    expect(data).toContain('<li>2nd')
    expect(data).toContain('<li>3')
    expect(headers['content-type']).toBe('text/html')
  })

  it('should execute a payload validation', async () => {
    const response = await makeLiveDD<FileDispatcherPayload>()
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })

})
