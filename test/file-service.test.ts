import FileDispatcher, { FileDispatcherPayload } from '../src/dispatcher/FileDispatcher'
import { AbstractData } from '../src/dispatcher/interfaces/generics'
import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import makeLiveDD from './live-tests/dd-live'
import makeDDStub from './mocks/dd-http-stub'

const FIXTURE_1 = {
  templateName: 'superadmincompany-admin-test-mail-1',
  resultFileName: 'fichier',
  data: {
    name: 'Foo',
    breadcrumb: ['first', '2nd', 3],
  },
}

describe('File service', () => {
  const fix = FIXTURE_1
  it('should set url, method and payload data', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.url.toString()).toMatch(/\/file$/u)
      expect(req.payload).toMatchObject(fix)
    })
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .dispatch()
  })

  it('should set the /validate path on validate', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(fix)
      expect(req.url.toString()).toMatch(/\/validate$/u)
    })
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .validateOnly()
      .dispatch()
  })

  it('ignores duplicate validation silently', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.url.toString()).toMatch(/\/validate$/u)
      expect(req.url.toString()).not.toMatch(/\/validate\/validate$/u)
    })
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .validateOnly()
      .validateOnly()
      .dispatch()
  })

  it('should execute a payload validation and succeeds', async () => {
    const response = await makeLiveDD<FileDispatcherPayload>()
      .withBaseService(new FileDispatcher()
        .setTemplateName(fix.templateName)
        .setResultFileName(fix.resultFileName)
        .setData(fix.data as AbstractData))
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })

  it('should execute a payload validation and handle the error', async () => {
    await makeLiveDD<FileDispatcherPayload>()
      .withBaseService(new FileDispatcher().setTemplateName(fix.templateName))
      .validateOnly()
      .dispatch()
      .then(
        _response => {
          expect(false).toBeTruthy()
        },
        err => {
          expect(err.response.status).toBe(417)
          expect(err.response.statusText).toBe('Expectation Failed')
        }
      )
  })
})
