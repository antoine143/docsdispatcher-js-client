import RequestDescriptor from '../src/http/interfaces/RequestDescriptor'
import makeDDStub from './mocks/dd-http-stub'
import PostalDispatcher, { PostalDispatcherPayload } from '../src/dispatcher/PostalDispatcher'
import UploadDispatcher from '../src/dispatcher/UploadDispatcher'
import { Attachment, Target } from '../src/dispatcher/interfaces/generics'
import makeLiveDD from './live-tests/dd-live'

function fixturePostalDispatcher(): PostalDispatcher {
  return new PostalDispatcher({ subject: 'Test postal LREL' })
    .addDocument(
      new Attachment({
        templateName: 'superadmincompany-admin-test-small-docx-1',
      })
    )
    .addDocument(
      new Attachment({
        templateName: 'superadmincompany-admin-test-small-docx-1',
        data: {
          name: 'Steve',
        },
      })
    )
    .addReceiver({
      name: 'M Dupont Francois',
      address1: 'Appartement 1',
      address2: 'Batiment 2',
      address3: '105 Boulevard Brune',
      address4: '',
      city: 'Paris',
      zipCode: '75014',
      countryCode: 'FR',
    })
    .addReceiver({
      name: 'M Dupont Claire',
      address1: 'Appartement 2',
      address2: 'Batiment 5',
      address3: '105 Boulevard Brune',
      address4: '',
      city: 'Paris',
      zipCode: '75014',
      countryCode: 'FR',
    })
    .setData({
      name: 'Lud',
    })
}

function fixtureMainTargets(): Target[] {
  return [{ target: 'ZohoCrm' }]
}

function fixtureDispatcherWithTargets(): PostalDispatcher {
  return fixturePostalDispatcher().addTargets(fixtureMainTargets())
}

const EXPECTED_1 = {
  provider: 'MYSENDINGBOX',
  subject: 'Test postal LREL',
  receivers: [
    {
      name: 'M Dupont Francois',
      address1: 'Appartement 1',
      address2: 'Batiment 2',
      address3: '105 Boulevard Brune',
      address4: '',
      city: 'Paris',
      zipCode: '75014',
      countryCode: 'FR',
    },
    {
      name: 'M Dupont Claire',
      address1: 'Appartement 2',
      address2: 'Batiment 5',
      address3: '105 Boulevard Brune',
      address4: '',
      city: 'Paris',
      zipCode: '75014',
      countryCode: 'FR',
    },
  ],
  documents: [
    {
      templateName: 'superadmincompany-admin-test-small-docx-1',
    },
    {
      templateName: 'superadmincompany-admin-test-small-docx-1',
      data: {
        name: 'Steve',
      },
    },
  ],
  data: {
    name: 'Lud',
  },
  colorMode: 'BW',
}

describe('Postal service', () => {
  it('should set url, method and payload data', () => {
    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.method).toBe('post')
      expect(req.url.toString()).toMatch(/\/postal$/u)
      expect(req.payload).toMatchObject(EXPECTED_1)
    })
      .withBaseService(fixturePostalDispatcher())
      .dispatch()
  })

  it('should set the /validate path on validate', () => {
    const payload = EXPECTED_1

    return makeDDStub<PostalDispatcherPayload>(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(payload)
      expect(req.url.toString()).toMatch(/\/validate$/u)
    })
      .withBaseService(fixturePostalDispatcher())
      .validateOnly()
      .dispatch()
  })

  it('can have a composed dispatcher', () => {
    const payload = EXPECTED_1
    // const targets = fixtureMainTargets()

    return makeDDStub(function (req: RequestDescriptor) {
      expect(req.payload).toMatchObject(payload)
      expect(req.url.toString()).toMatch(/\/upload$/u)
      // expect(data.targets).toEqual(targets)
    })
      .withBaseService(fixtureDispatcherWithTargets())
      .compose(new UploadDispatcher())
      .dispatch()
  })

  it('should execute a payload validation and succeeds', async () => {
    const response = await makeLiveDD<PostalDispatcherPayload>()
      .withBaseService(fixturePostalDispatcher())
      .validateOnly()
      .dispatch()
    expect(response.status).toBe(200)
    expect(response.data).toBe('')
  })
})
